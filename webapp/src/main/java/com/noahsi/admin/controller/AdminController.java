package com.noahsi.admin.controller;

import com.noahsi.service.MasterService;
import jakarta.annotation.Resource;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotNull;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.noahsi.common.base.controller.BaseController;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/admin")
public class AdminController extends BaseController {

  @Resource
  private MasterService masterService;


  @RequestMapping(value = "/upload", method = RequestMethod.POST)
  public ResponseEntity<Void> upload(@RequestParam(value = "file", required = false) MultipartFile[] file) {
    masterService.upload(file);
    return new ResponseEntity<>(HttpStatus.OK);
  }
}
