package com.noahsi.service;

import org.springframework.web.multipart.MultipartFile;

public interface MasterService {

    void upload(MultipartFile[] files);
}
