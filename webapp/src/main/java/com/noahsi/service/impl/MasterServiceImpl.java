package com.noahsi.service.impl;


import com.noahsi.common.constant.PropertiesReader;
import com.noahsi.common.exception.CustomResponseException;
import com.noahsi.common.exception.message.MessageEnum;
import com.noahsi.service.MasterService;
import com.noahsi.common.utils.ToolUtil;
import jakarta.annotation.Resource;
import java.io.File;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.multipart.MultipartFile;

@Service
public class MasterServiceImpl implements MasterService {

  private static final Logger logger = LoggerFactory.getLogger(MasterServiceImpl.class);

  @Resource
  PropertiesReader propertiesReader;

  @Override
  public void upload(MultipartFile[] files) {
    if (files == null) {
      logger.warn("files is null.");
      return;
    }

    for (MultipartFile file : files) {
      String path = save(file);

      unZip(path);
    }
  }

  private String save(MultipartFile file) {
    try {
      File destFile = new File(propertiesReader.getZipPath() + file.getOriginalFilename());

      if (!destFile.getParentFile().exists()) {
        File parentDir = new File(destFile.getParent());
        parentDir.mkdirs();
      }

      file.transferTo(destFile.getAbsoluteFile());
      return destFile.getAbsolutePath();
    } catch (Exception e) {
      logger.error("Upload failed.", e);
      throw new CustomResponseException(HttpStatus.INTERNAL_SERVER_ERROR, MessageEnum.E30002);
    }
  }

  private void unZip(String path) {
    try {
      ToolUtil.decompress(path, "UsrApp.log", propertiesReader.getLogPath());
    } catch (Exception e) {
      logger.error("Unzip failed.", e);
      throw new CustomResponseException(HttpStatus.INTERNAL_SERVER_ERROR, MessageEnum.E30001);
    }
  }

}
