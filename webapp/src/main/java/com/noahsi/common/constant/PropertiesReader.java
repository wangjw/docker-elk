package com.noahsi.common.constant;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
public class PropertiesReader {

  // パス
  @Value("${zip.path}")
  private String zipPath;


  // パス
  @Value("${log.path}")
  private String logPath;
}
