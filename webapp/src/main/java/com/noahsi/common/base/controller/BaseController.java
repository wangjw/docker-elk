package com.noahsi.common.base.controller;


import jakarta.annotation.Resource;
import jakarta.servlet.http.HttpServletResponse;

public class BaseController {

  @Resource
  protected HttpServletResponse response;

}
