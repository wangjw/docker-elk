package com.noahsi.common.exception;

import com.noahsi.common.utils.SpringContextUtil;
import java.util.ArrayList;
import java.util.List;
import com.noahsi.common.exception.message.ApiErrorResponse;
import com.noahsi.common.exception.message.MessageEnum;
import com.noahsi.common.message.service.MessageService;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;

@Data
public class CustomResponseException extends RuntimeException {

  private HttpStatus status;
  private ApiErrorResponse response = new ApiErrorResponse();

  private MessageService messageService;

  public CustomResponseException(HttpStatus status, MessageEnum messageEnum) {
    this.messageService = SpringContextUtil.getBean(MessageService.class);
    this.status = status;
    this.response.setCode(messageEnum.toString());
    this.response.setMessage(messageService.getMessageEntity(messageEnum));
  }

  public CustomResponseException(HttpStatus status, MessageEnum messageEnum, String... parameter) {
    this.messageService = SpringContextUtil.getBean(MessageService.class);
    this.status = status;
    this.response.setCode(messageEnum.toString());
    this.response.setMessage(messageService.getMessageEntity(messageEnum, parameter));
  }

  /**
   * add error.
   *
   * @param messageEnum message
   */
  public void addError(MessageEnum messageEnum, String... parameter) {
    String content = messageService.getMessageEntity(messageEnum, parameter);

    List<String> details = response.getDetails();

    if (details == null) {
      details = new ArrayList<>();
      details.add(content);
      response.setDetails(details);
    } else {
      details.add(content);
    }
  }

  public boolean hasError() {
    return !CollectionUtils.isEmpty(response.getDetails());
  }

}
