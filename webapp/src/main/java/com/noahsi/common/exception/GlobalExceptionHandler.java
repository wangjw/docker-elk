package com.noahsi.common.exception;

import com.noahsi.Application;
import com.noahsi.common.exception.message.ApiErrorResponse;
import com.noahsi.common.exception.message.MessageEnum;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import org.springframework.web.servlet.resource.NoResourceFoundException;


@RestControllerAdvice
public class GlobalExceptionHandler {
  private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

  /**
   * リクエストメソッドを存在しないエラーを処理.
   */
  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  public ResponseEntity<ApiErrorResponse> handleHttpRequestMethodNotSupportedException(
      HttpRequestMethodNotSupportedException e) {
    log.error("handleHttpRequestMethodNotSupportedException: ", e);
    CustomResponseException customResponseException = new CustomResponseException(
        HttpStatus.METHOD_NOT_ALLOWED, MessageEnum.E00002);
    return new ResponseEntity<>(customResponseException.getResponse(),
        HttpStatus.METHOD_NOT_ALLOWED);
  }

  /**
   * リクエストメディアタイプを存在しないエラーを処理.
   */
  @ExceptionHandler(NoResourceFoundException.class)
  public ResponseEntity<ApiErrorResponse> handleNoResourceFoundException(NoResourceFoundException e) {
    log.error("handleNoResourceFoundException: ", e);
    CustomResponseException customResponseException = new CustomResponseException(
        HttpStatus.NOT_FOUND, MessageEnum.E00002);
    return new ResponseEntity<>(customResponseException.getResponse(), HttpStatus.NOT_FOUND);
  }

  /**
   * リクエストデータ型は不正のエラーを処理.
   */
  @ExceptionHandler(HttpMessageNotReadableException.class)
  public ResponseEntity<ApiErrorResponse> handleHttpMessageNotReadableException(
      HttpMessageNotReadableException e) {
    log.error("handleHttpMessageNotReadableException: ", e);
    CustomResponseException customResponseException = new CustomResponseException(
        HttpStatus.BAD_REQUEST, MessageEnum.E00003);
    return new ResponseEntity<>(customResponseException.getResponse(), HttpStatus.BAD_REQUEST);
  }

  /**
   * メソッドのパラメータのエラーを処理.
   *
   * @param e エラー
   * @return ModelAndView エラー処理リクエスト
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ApiErrorResponse> handleMethodArgumentNotValidException(
      MethodArgumentNotValidException e) {
    log.error("handleMethodArgumentNotValidException: ", e);
    CustomResponseException customResponseException = new CustomResponseException(
        HttpStatus.BAD_REQUEST, MessageEnum.E20001);
    BindingResult bindingResult = e.getBindingResult();
    List<ObjectError> allErrors = bindingResult.getAllErrors();
    for (ObjectError fieldError : allErrors) {
      String message = fieldError.getDefaultMessage();
      if (!StringUtils.hasText(message)) {
        continue;
      }
      String[] messages = message.split(",");
      customResponseException.addError(MessageEnum.valueOf(messages[0]), getMessageParas(messages));
    }

    return new ResponseEntity<>(customResponseException.getResponse(), HttpStatus.BAD_REQUEST);
  }

  /**
   * カスタム例外.
   */
  @ExceptionHandler(CustomResponseException.class)
  public ResponseEntity<ApiErrorResponse> handleException(CustomResponseException exception) {
    return new ResponseEntity<>(
        exception.getResponse(), exception.getStatus());
  }

  /**
   * 汎用エラーを処理.
   */
  @ExceptionHandler(value = Exception.class)
  public ResponseEntity<ApiErrorResponse> handleException(Exception e) {
    log.error("handleException: ", e);
    CustomResponseException customResponseException = new CustomResponseException(
        HttpStatus.INTERNAL_SERVER_ERROR, MessageEnum.E00001);
    return new ResponseEntity<>(customResponseException.getResponse(),
        HttpStatus.INTERNAL_SERVER_ERROR);
  }

  /**
   * メッセージのパラメータを取得する.
   *
   * @param messages メッセージアレイ
   * @return String[] メッセージのパラメータアレイ
   */
  private String[] getMessageParas(String[] messages) {
    // リストを作成
    List<String> paraList = new ArrayList<>();
    // リストにメッセージアレイを追加
    Collections.addAll(paraList, messages);
    // インデックス「0」のメッセージを削除
    paraList.remove(0);
    // メッセージパラメータアレイを作成
    return paraList.toArray(new String[0]);
  }
}
