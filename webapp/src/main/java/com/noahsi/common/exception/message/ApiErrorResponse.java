package com.noahsi.common.exception.message;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import java.util.List;
import lombok.Data;

@JsonInclude(Include.NON_NULL)
@Data
public class ApiErrorResponse {

  /** エラー種類. */
  private String code;

  /** メッセー. */
  private String message;

  /** エラーリスト. */
  private List<String> details;
}
