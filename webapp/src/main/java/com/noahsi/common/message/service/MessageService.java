package com.noahsi.common.message.service;

import com.noahsi.common.exception.message.MessageEnum;
import jakarta.annotation.Resource;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

@Service
public class MessageService {

  // springメッセージソース
  @Resource
  private MessageSource messageSource;

  /**
   * メッセージ詳細を取得.
   *
   * @param messageEnum メッセージ
   * @param parameter         パラメータ
   * @return MessageDetail メッセージ詳細を取得
   */
  public String getMessageEntity(MessageEnum messageEnum, String... parameter) {
    // メッセージコード存在する場合
    if (messageEnum != null) {
      return messageSource.getMessage(
          messageEnum.name(), parameter, LocaleContextHolder.getLocale());
    }
    return "";
  }
}
