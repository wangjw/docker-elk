package com.noahsi.common.config;


import java.nio.charset.StandardCharsets;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;

@Configuration
public class RootContextConfiguration {

  @Bean
  public MessageSource messageSource() {
    ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
    try {
      messageSource.setCacheSeconds(30);
      messageSource.setDefaultEncoding(StandardCharsets.UTF_8.name());
      messageSource.addBasenames("classpath:i18n/messages");
    } catch (Exception e) {
      throw new RuntimeException("Create messageSource error.", e);
    }
    return messageSource;
  }

}
