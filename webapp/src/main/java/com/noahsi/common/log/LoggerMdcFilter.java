package com.noahsi.common.log;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;


import org.slf4j.MDC;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

@Component
@Order(1)
public class LoggerMdcFilter extends OncePerRequestFilter {

  /**
   * UUID.
   */
  private static final String LOG_PARAM_UUID = "uuid";

  /**
   * userId.
   */
  private static final String LOG_PARAM_USER_ID = "userId";
  /**
   * deviceId.
   */
  private static final String LOG_PARAM_DEVICE_ID = "deviceId";
  /**
   * method.
   */
  private static final String LOG_PARAM_METHOD = "method";
  /**
   * uri.
   */
  private static final String LOG_PARAM_URI = "uri";
  /**
   * ip.
   */
  private static final String LOG_PARAM_IP = "ip";

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    // UUID（API処理単位ごとに付与）
    MDC.put(LOG_PARAM_UUID, UUID.randomUUID().toString());

    // BFF API-ID名
    String method = request.getMethod();
    String uri = request.getRequestURI();
    String userId = request.getHeader("login-user-id");
    String deviceId = request.getHeader("device-id");
    String ip = getIpAddress(request);

    MDC.put(LOG_PARAM_DEVICE_ID, deviceId);
    MDC.put(LOG_PARAM_URI, uri);
    MDC.put(LOG_PARAM_METHOD, method);
    MDC.put(LOG_PARAM_USER_ID, userId);
    MDC.put(LOG_PARAM_IP, ip);
    try {
      filterChain.doFilter(request, response);

    } finally {
      MDC.clear();
    }
  }

  public String getIpAddress(HttpServletRequest request) {
    String ip = request.getHeader("x-forwarded-for");
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getRemoteAddr();
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("WL-Proxy-Client-IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("HTTP_CLIENT_IP");
    }
    if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
      ip = request.getHeader("HTTP_X_FORWARDED_FOR");
    }
    return ip;
  }
}
