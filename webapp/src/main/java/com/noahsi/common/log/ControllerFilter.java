package com.noahsi.common.log;

import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.AntPathMatcher;
import org.springframework.util.PathMatcher;
import org.springframework.util.StreamUtils;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingResponseWrapper;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@Component
@Order(2)
public class ControllerFilter extends OncePerRequestFilter {

  private static final Logger LOGGER = LoggerFactory
      .getLogger(ControllerFilter.class);

  // 長さが10 Kを超える場合、内容はINFOログに出力されません。
  private static final int CONTENT_MAX_SIZE = 10 * 1024;

  /**
   * リクエスト／レスポンス内容を記録する.
   *
   * @param request     リクエスト
   * @param response    レスポンス
   * @param filterChain filterChain
   * @throws ServletException ServletException
   * @throws IOException      IOException
   */
  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    String contentType = request.getContentType();

    ContentCachingResponseWrapper responseWrapper = new ContentCachingResponseWrapper(response);

    if (contentType.startsWith("multipart/")) {
      Map<String, Object> requestMap = getRequestMap(request);
      LOGGER.info("Request：{}", convent2Json(requestMap));
      filterChain.doFilter(request, response);
    } else {
      CachedBodyHttpServletRequest requestWrapper = new CachedBodyHttpServletRequest(request);
      Map<String, Object> requestMap = getRequestMap(requestWrapper);
      LOGGER.info("Request：{}", convent2Json(requestMap));

      filterChain.doFilter(requestWrapper, responseWrapper);
    }

    Map<String, Object> responseMap = getResponseMap(response, responseWrapper);
    LOGGER.info("Response {}", convent2Json(responseMap));
    responseWrapper.copyBodyToResponse();
  }

  /**
   * レスポンスに記録すべき内容を取得する.
   *
   * @param httpServletResponse レスポンス
   * @param responseWrapper     ラッピングされたレスポンス
   * @return 記録すべき内容
   * @throws IOException IOException
   */
  private Map<String, Object> getResponseMap(
      HttpServletResponse httpServletResponse, ContentCachingResponseWrapper responseWrapper)
      throws IOException {
    Map<String, Object> responseMap = new HashMap<>();
    // 大量INFO以上のログを出力しない。
    if (responseWrapper.getContentSize() <= CONTENT_MAX_SIZE) {
      responseMap.put(
          "Body",
          IOUtils.toString(
              responseWrapper.getContentInputStream()));
    }

    HashMap<String, Object> responseHeaders = new HashMap<>();
    for (String headerName : responseWrapper.getHeaderNames()) {
      responseHeaders.put(headerName, responseWrapper.getHeaders(headerName));
    }
    responseMap.put("Headers", responseHeaders);
    responseMap.put("Status", httpServletResponse.getStatus());
    return responseMap;
  }

  /**
   * リクエストに記録すべき内容を取得する.
   *
   * @param httpServletRequest ラッピングされたリクエスト
   * @return 記録すべき内容
   * @throws IOException IOException
   */
  private Map<String, Object> getRequestMap(
      CachedBodyHttpServletRequest httpServletRequest)
      throws IOException {
    Map<String, Object> requestMap = new HashMap<>();
    requestMap.put("URL", httpServletRequest.getRequestURL());
    requestMap.put("Method", httpServletRequest.getMethod());
    requestMap.put("Protocol", httpServletRequest.getProtocol());

    Map<String, Object> requestHeaders = new HashMap<>();
    Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String headerName = headerNames.nextElement();
      Enumeration<String> headers = httpServletRequest.getHeaders(headerName);
      List<String> values = new ArrayList<>();
      while (headers.hasMoreElements()) {
        values.add(headers.nextElement());
      }
      requestHeaders.put(headerName, values);
    }
    requestMap.put("headers", requestHeaders);

    List<Map<String, String>> parameterList = new ArrayList<>();
    Map<String, String> parameterMaps = new HashMap<>();
    for (Enumeration<String> names = httpServletRequest.getParameterNames();
        names.hasMoreElements(); ) {
      String name = names.nextElement();
      parameterMaps.put(name, httpServletRequest.getParameter(name));
    }
    parameterList.add(parameterMaps);
    requestMap.put("Parameters", parameterList);

    InputStream inputStream = httpServletRequest.getInputStream();
    byte[] body = StreamUtils.copyToByteArray(inputStream);
    String bodyStr = new String(body, httpServletRequest.getCharacterEncoding());
    if (bodyStr.length() <= CONTENT_MAX_SIZE) {
      requestMap.put("Body", bodyStr);
    }
    return requestMap;
  }

  private Map<String, Object> getRequestMap(
      HttpServletRequest httpServletRequest)
      throws IOException {
    Map<String, Object> requestMap = new HashMap<>();
    requestMap.put("URL", httpServletRequest.getRequestURL());
    requestMap.put("Method", httpServletRequest.getMethod());
    requestMap.put("Protocol", httpServletRequest.getProtocol());

    Map<String, Object> requestHeaders = new HashMap<>();
    Enumeration<String> headerNames = httpServletRequest.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String headerName = headerNames.nextElement();
      Enumeration<String> headers = httpServletRequest.getHeaders(headerName);
      List<String> values = new ArrayList<>();
      while (headers.hasMoreElements()) {
        values.add(headers.nextElement());
      }
      requestHeaders.put(headerName, values);
    }
    requestMap.put("headers", requestHeaders);

    List<Map<String, String>> parameterList = new ArrayList<>();
    Map<String, String> parameterMaps = new HashMap<>();
    for (Enumeration<String> names = httpServletRequest.getParameterNames();
        names.hasMoreElements(); ) {
      String name = names.nextElement();
      parameterMaps.put(name, httpServletRequest.getParameter(name));
    }
    parameterList.add(parameterMaps);
    requestMap.put("Parameters", parameterList);
    return requestMap;
  }

  /**
   * オブジェクトをjson形式に変換する.
   *
   * @param o オブジェクト
   * @return json形式
   */
  private String convent2Json(Object o) {
    ObjectMapper mapper = new ObjectMapper();
    try {
      return mapper.writeValueAsString(o);
    } catch (JsonProcessingException e) {
      return String.valueOf(o);
    }
  }
}
