package com.noahsi.common.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class ToolUtil {

  /**
   * ZIPファイル解凍
   * @param srcPath ZIPファイルパス
   * @param destFile 解凍ファイル
   * @param destPath 解凍パス
   * @throws Exception 解凍Exception
   */
  public static void decompress(String srcPath, String destFile, String destPath) throws Exception {
    try (ZipFile zf = new ZipFile(new File(srcPath), Charset.forName("MS932"))) {
      Enumeration<? extends ZipEntry> entries = zf.entries();
      ZipEntry entry;
      while (entries.hasMoreElements()) {
        entry = entries.nextElement();

        if (entry.isDirectory()) {
          continue;
        }

        if (!entry.getName().endsWith(destFile)) {
          continue;
        }

        File file = new File(destPath + "/" + entry.getName().replaceAll("/", "_"));

        if (!file.exists() && !file.getParentFile().exists()) {
          File parentDir = new File(file.getParent());
          parentDir.mkdirs();
        }

        try (InputStream is = zf.getInputStream(entry);
            FileOutputStream fos = new FileOutputStream(file)) {
          int count;
          byte[] buf = new byte[8192];
          while ((count = is.read(buf)) != -1) {
            fos.write(buf, 0, count);
          }
        }
      }
    }
  }

}
